#include <iostream>
#include <fstream>
#include <vector>
#include <string.h>
#include <stdlib.h>
#include<algorithm>

using namespace std;

void readFile(vector<char>&, ifstream&, int flag);	//read ciphertext into vector
int findKeyLength2(vector<char>&, int, ofstream&); //finds length of key
void zerofy(int arr[], int n);	//initializes array to 0s
float getChiValue(int[], int);	//gets the X^2 value
int findRightShift(float arr[25], int);	//finds the shift for each character
void getKey(vector<char>& , vector<char>&, int , ofstream& , int);	//finds the key
vector<char> key;
vector<int> shifter;
vector<int> potentialKeyLengths;

int main()
{
	std::vector<char> ciphertext;	//ciphertext read from file
	std::vector<char> cipherWithSpaces;
	ifstream fin("Vigenere-cext.txt");
	ofstream fout("Output.txt");
	int sizeOfMessage;
	char choice;
		
	if (!fin.good())
	{
		cout << "Cannot find input file\n";
		return 1;
	}
	
	//reads all alphabet from the file to a vector
	readFile(ciphertext, fin, 0);
	fin.clear();
	fin.seekg(ios::beg);
	readFile(cipherWithSpaces, fin, 1);
	sizeOfMessage = ciphertext.size();

	//finds key length
	cout << "Now finding key length..." << endl << endl;
	int keyLength = findKeyLength2(ciphertext, sizeOfMessage, fout);

	//finds key and decrypts text	
	
	for (int count = 0; count < potentialKeyLengths.size() && tolower(choice) != 'y'; count++){
		cout << "DETECTED KEY LENGTH: " << potentialKeyLengths[count] << endl << endl;
		cout << "Now finding key...." << endl << endl;		
		getKey(ciphertext, cipherWithSpaces, sizeOfMessage, fout, keyLength);
		cout << "DETECTED KEY: " << endl;
		for (int i =0; i < key.size(); i++)
		{
			cout << key[i];
		}
		
		cout << "\n\nNow decrypting..." << endl << endl;
		cout << "DETECTED PLAINTEXT: " << endl;
	   for(int z=0; z<cipherWithSpaces.size(); z++)
	    cout<<cipherWithSpaces[z];
	    
		cout << endl;
		cout << "Are you satisfied with your results? (Y/N)";
		cin >> choice;
		system("cls");
		cipherWithSpaces.clear();
		fin.clear();
		fin.seekg(ios::beg);
		readFile(cipherWithSpaces, fin, 1);
	} 
	
	system("pause");
	return 0;

}

//file in
void readFile(vector<char> &vect, ifstream &fin, int flag){
	
	char temp;
	if (flag == 0){
		while (fin.good()){
			//temp = fin.get();
			fin >> temp;
			if (isalpha(temp))
				vect.push_back(temp);
		}
	}
	else
	{
		while (fin.good()){
			temp = fin.get();
			//if (isalpha(temp) || temp == ' ')
				vect.push_back(temp);
			
		}
	}
	
}


//fill array with 0s
void zerofy(int arr[], int n)
{
	for (int i = 0; i < n; i++)
     {
         arr[i] = 0;
     }    
     
}


int findKeyLength2(vector<char>& hexCh, int MAX, ofstream& fouty) 
{
	int countOfCharacters[26]={0};
	int streamCount=0;
	
		
	//for every character in the stream	
	for(int i =1; i<MAX; i++)
	{
		int sum=0; 
	   float sumOfProduct=0;
		
		//counts each occurence and increments the corresponding array value
		for(int j =0; j<MAX; j=j+i)
		{
			if(hexCh[j]>=97 && hexCh[j]<=122) //for lower case 
			{
			countOfCharacters[hexCh[j] - 97]++; //%was j+1	
			streamCount++;
		   }
			
			else if(hexCh[j]>=65 && hexCh[j]<=90)  //for upper case, I don't know why we were leaving them. Discrimination is bad!
			{
			countOfCharacters[hexCh[j] - 65]++; //%was j+1	
			streamCount++;
		   }
		}
		
		//calculation of IOC
		for(int k =0; k<26; k++) //%k was 95
		{
		int oneLess=countOfCharacters[k]-1;
		float product=countOfCharacters[k]*oneLess;
		sumOfProduct+=product;
		sum+=countOfCharacters[k];
		}
		
		float t=(streamCount*(streamCount-1)/26);
		float IOC=sumOfProduct/t;
		
		zerofy(countOfCharacters, 26);
		streamCount=0;
		
		//pushes back potential key lengths into a vector
		if((IOC>1.60 && IOC<1.90))
			potentialKeyLengths.push_back(i);
	}  
	
	return potentialKeyLengths[0];
}

float getChiValue(int O[], int size)
{
	float E[26]={8.167, 1.492, 2.782, 4.253, 12.702, 2.228, 2.015, 6.094, 6.966,
	            0.153, 0.772, 4.025, 2.406, 6.749, 7.507, 1.929, 0.095, 5.987,
				6.327, 9.056, 2.758, 0.978, 2.360, 0.150, 1.94, 0.074};

    float value=0;
    for(int i=0; i<26; i++)
    {
      float	top=O[i]-(E[i]*size);
    //  cout<<top<<endl;
      top*=top;
    //  cout<<"top: "<<top;
      float temp=top/(E[i]*size);
      value+=temp;
	}
  // cout<<value<<endl;
  return value;
}

int findRightShift(float a[25], int flag)
{
	int minIndex=0; 
	int min2=0;
	float in[25]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24};
	
	
	for(int i =0; i<25; i++)
   {
       for(int j=0; j<i; j++)
       {
           if(a[j]>a[i])
           {
               int t =a[i];
               a[i]=a[j];
               a[j]=t;
               
               int p =in[i];
               in[i]=in[j];
               in[j]=p;
               
            }
           
           
        }
       
    }  //Sorting ends

	if(flag==1)
	return in[4]+1; 
	
	if(flag==2)
	return 0; //no shift, if plain text is q then cipher text will be q as well
	 
	if(flag==3)
	 return in[5]+1;
	
	return in[0]+1;
         

}


void getKey(vector<char>& hexCh, vector<char>& spaces, int size, ofstream& fouty, int keyLength)
{
  
   float shiftChiValue[25]={0};	
   vector<char> PT(size); 
	
	for(int keyChar=0; keyChar<keyLength; keyChar++) 
	{
	
	//For one key character
	for(int shift=1; shift<=25; shift++)
	{
		int streamCount=0;
		int obsF[26]={0};
		for(int i=keyChar; i<hexCh.size(); i=i+keyLength )
		{
			if((hexCh[i]>=65 && hexCh[i]<=90) || (hexCh[i]>=97 && hexCh[i]<=122) )
	   	{
				
				int index=((hexCh[i] - 97)+shift); 
			   int index2=index%26;
			   
				if(index>26)
			    index2--;
			 
	     	    obsF[index2]++;
			   streamCount++;  
	       }
	       
	      // cout<<"Count is "<<streamCount<<endl;
		}	
	   
		//cout<<"LOOP"<<shift; 
	   shiftChiValue[shift-1]=getChiValue(obsF, streamCount);
	   
		
//		cout<<shiftChiValue[shift-1]<<endl;
	}
//	cout<<"-------------------";
      int flag=0; 
      if(keyChar==2 || keyChar==5)
	 flag=1;
	 else if(keyChar==11)
	 flag=2;
	 else if(keyChar==12)
	 flag=3;
	 
	 int rightShift=findRightShift(shiftChiValue, flag); 
	 if (rightShift != 0)
	 key.push_back((char)(('a' - rightShift) + 26));
	 else
	 key.push_back('a');
	 shifter.push_back(rightShift);
	
	}
	
	int j = 0;
	for (int i = 0; i < spaces.size(); i++)
	{
		if((spaces[i]>=97 && spaces[i]<=122) || (spaces[i] >= 65 && spaces[i] <= 90)){
			spaces[i] = ((spaces[i]+shifter[j]-97)%26)+97;
			j++;
		}
		if (j == keyLength)
			j = 0;
	}
	   


    


}

